﻿using System;

public class Class1
{
    static void Main(string[] args)
    {
        Console.Write("Введите начальное значение диапазона: ");
        string ArrBegin = Console.ReadLine();
        int i = Convert.ToInt32(ArrBegin);

        Console.Write("Введиет конечное значение диапазона: ");
        string ArrEnd = Console.ReadLine();
        int j = Convert.ToInt32(ArrEnd);

        for (; i < j; i++)
        {
            if (i % 15 == 0)
                Console.Write("{0} ", "FizzBuzz");
            else if (i % 3 == 0)
                Console.Write("{0} ", "Fizz");
            else if (i % 5 == 0)
                Console.Write("{0} ", "Buzz");
            else
                Console.Write("{0} ", i);
        }
    }
}
