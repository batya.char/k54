namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] collection = new int[42];
            Random rand = new Random();
            for (int i = 0; i < collection.Length; i++)
                collection[i] = rand.Next(0,200);
            List<string> StartArray = FizzBuzz(collection);
            foreach (var i in StartArray)
            {
                Console.WriteLine(i);
            }
        }
        public static List<string> FizzBuzz(int[] array)
        {

            List<string> FIZZBUZZ = new List<string>();
            foreach (int i in array)
            {
                if (i % 3 == 0 && i % 5 == 0)
                {
                    FIZZBUZZ.Add("FizzBuzz");
                }
                else if (i % 3 == 0)
                {
                    FIZZBUZZ.Add("Fizz");
                }
                else if (i % 5 == 0)
                {
                    FIZZBUZZ.Add("Buzz");
                }
                else
                {
                    FIZZBUZZ.Add(i.ToString());
                }
            }          
            return FIZZBUZZ;
        }
    }
