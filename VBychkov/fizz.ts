let a: Array<number> = [13, 36, 64, 33, 18, 3, 35, 77, 16, 25,63, 20, 30, 65, 94, 68, 67, 37, 97]

const replaceNumberToFizzBuzz: Function = (array: Array<number>) :Array<number|string> => {
    array.forEach((value: number, index: number) => {
        if (+value % 15 === 0) {
            a[index+''] = 'FizzBuzz'
        } else if (+value % 3 === 0) {
            a[index+''] = "Fizz"
        } else if (+value % 5 === 0) {
            a[index+''] = 'Buzz'
        }

    })
    return array
}

console.log(replaceNumberToFizzBuzz(a))