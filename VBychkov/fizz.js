var a = [13, 36, 64, 33, 18, 3, 35, 77, 16, 25, 63, 20, 30, 65, 94, 68, 67, 37, 97];
var replaceNumberToFizzBuzz = function (array) {
    array.forEach(function (value, index) {
        if (+value % 15 === 0) {
            a[index + ''] = 'FizzBuzz';
        }
        else if (+value % 3 === 0) {
            a[index + ''] = "Fizz";
        }
        else if (+value % 5 === 0) {
            a[index + ''] = 'Buzz';
        }
    });
    return array;
};
console.log(replaceNumberToFizzBuzz(a));
