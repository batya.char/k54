package zhdanbb;

import java.util.ArrayList;

public class FizzBuzzClass {

    public static ArrayList FizzBuzz(FizzBuzzObject object){

        ArrayList mainList = new ArrayList<>();

        for (int i : object.chainArray){
            if(i % object.firstValue == 0 && i % object.secondValue == 0){
                mainList.add(object.thirdWord);
            }
            else if (i % object.secondValue == 0){
                mainList.add(object.secondWord);
            }
            else if (i % object.firstValue == 0){
                mainList.add(object.firstWord);
            }
            else{
                mainList.add(i);
            }
        }

        return mainList;
    }
}
