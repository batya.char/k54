package zhdanbb;

import java.util.ArrayList;
import java.util.Random;

import static zhdanbb.FizzBuzzClass.FizzBuzz;

public class UseClass {

    public static void main(String[] args) {

        int[] setValue = new int[10];
        Random random = new Random();

        for (int i = 0; i < setValue.length; i++){
            setValue[i] = random.nextInt(100);
        }

        FizzBuzzObject mainObject = new FizzBuzzObject();

        mainObject.firstValue = 3;
        mainObject.secondValue = 5;
        mainObject.firstWord = "Fizz";
        mainObject.secondWord = "Buzz";
        mainObject.thirdWord = "FizzBuzz";
        mainObject.chainArray = setValue;

        ArrayList getValue = FizzBuzz(mainObject);
        getValue.forEach(System.out::println);
    }

}
