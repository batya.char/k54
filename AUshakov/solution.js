function FizzBuzzFunc(argList){
  
  return argList.map((item) => {
    if (item % 5 === 0 && item % 3 === 0) {
      return 'FizzBuzz'
    } else if (item % 3 === 0){
      return 'Fizz'
    } else if (item % 5 === 0){
      return 'Buzz'
    } else {
      return item
    }

  });

}

console.log(FizzBuzzFunc([4,3,5,3,15]))