package AErmakov;

import java.util.stream.IntStream;

public class buzz {
    public static void main(String[] args){
        IntStream.of(120, 7, 85, 32, 314, 12, 6, 3, 15)
                .mapToObj(i -> i % 3 == 0 ? (i % 5 == 0 ? "FizzBuzz" : "Fizz") : (i % 5 == 0 ? "Buzz" : i))
                .forEach(System.out::println);
    }
}
