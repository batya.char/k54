﻿using System;

public class Class1
{
    static void Main(string[] args)
    {
        int num;
        string check;

        var word = new List<string>() { "FizzBuzz", "Fizz", "Buzz" };
        var result = new List<string>();
        var number = new List<int>();

        for (; ; )
        {
            check = Console.ReadLine();
            if (check == "s")
                break;
            else
            {
                try
                {
                    num = Convert.ToInt32(check);
                    number.Add(num);
                }
                catch
                {
                    Console.WriteLine("Ошибка");
                }
            }
        }

        for (int numbers = 0; numbers < number.Count; numbers++)
        {
            if (number[numbers] % 3 == 0 && number[numbers] % 5 == 0)
                result.Add(word[0]);
            else if (number[numbers] % 3 == 0)
                result.Add(word[1]);
            else if (number[numbers] % 5 == 0)
                result.Add(word[2]);
            else
                result.Add(Convert.ToString(number[numbers]));
        }

        for (int i = 0; i < result.Count; i++)
            Console.WriteLine(result[i]);

        Console.ReadKey();
    }
}
