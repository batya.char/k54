const numbers = [3,5,15,2,10,30,60,6,43,643,24,0];

const FizzBuzzResult = FizzBuzz(numbers);

function FizzBuzz(mass){
    return mass.map((item) =>{

        if(item % 3 === 0 && item % 5 ===0){
            return 'FizzBuzz';
        }
        else if(item % 3 === 0){
            return 'Fizz';
        }
        else if(item % 5 === 0){
            return 'Buzz';
        }
        else{
            return item;
        }
    })
}

console.log(FizzBuzzResult);

