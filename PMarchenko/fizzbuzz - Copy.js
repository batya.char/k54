function j(g){
    var whitePeople = []
    for(i = 0; i < g.length; i++){
      var n = g[i]
  
      if(n % 5 === 0 && n % 3 === 0){
        whitePeople.push('FizzBuzz')
        continue
      }
  
      if(n % 3 === 0){
        whitePeople.push('Fizz')
        continue
      }
  
      if(n % 5 === 0){
        whitePeople.push('Buzz')
        continue
      }
  
      whitePeople.push(n)
    }
    return whitePeople
  }
  
  console.log(j([3,5,3,15]))