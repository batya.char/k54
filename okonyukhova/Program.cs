﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[] { 1, 2, 3, 4, 5, 15 };

            for(int i = 0; i < arr.Length; i++)
            {     
                if (arr[i] % 3 == 0 && arr[i] % 5 == 0)
                {                  
                    Console.WriteLine("FizzBuzz");
                }
                else if (arr[i] % 3 == 0)
                {
                    Console.WriteLine("Fizz");
                }
                else if (arr[i] % 5 == 0)
                {
                    Console.WriteLine("Buzz");
                }
                else
                {
                    Console.WriteLine(arr[i]);
                }  
            }
                
                
            
            Console.ReadKey();
        }
    }
}
