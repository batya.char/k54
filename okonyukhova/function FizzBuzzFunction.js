function FizzBuzzFunction(arr){

    return arr.map((item) => {
        if (item % 3 === 0 && item % 3 === 0){
            return 'FizzBuzz'
        } else if (item % 3 === 0){
            return 'Fizz'
        } else if (item % 5 === 0){
            return 'Buzz'
        } else {
            return item
        }
    });
}

let arr = [1, 3, 4, 5, 15, 17, 30];

console.log(FizzBuzzFunction(arr));